# aws-vpc-rds-eks

### Tool Used
Using Cloudformation for deploy:
- Deploy two VPC
- Setup peering between them
- Create rds mysql
- Create eks

### How to run

* Step 1: Create bucket cf-test on s3, and push all file to s3.

* Step 2: Upload all file to s3.

* Step 3: Create cloudformation stack and run with main.yml file.