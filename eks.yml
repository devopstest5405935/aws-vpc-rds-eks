AWSTemplateFormatVersion: '2010-09-09'
Description: EKS cluster for devops test
Parameters:
  VPC1:
    Type: String
    Description: The VPC for EKS
  VPC1PrivateSubnet1:
    Type: String
    Description: The subnet1 for the EKS
  VPC1PrivateSubnet2:
    Type: String
    Description: The subnet2 for the EKS
  EKSClusterName:
    Type: String
    Description: Name of k8s cluster
    Default: eks-cluster
  NumWorkerNodes:
    Type: Number
    Description: Number of worker nodes to create
    Default: 2
  WorkerNodesInstanceType:
    Type: String
    Description: EC2 instance type for the worker nodes
    Default: t3.medium
Mappings:
  # IDs of the "EKS-optimised AMIs" for the worker nodes:
  # https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html
  # IMPORTANT NOTE: Choose AWS EKS compatible ami IDs only
  EksAmiIds:
    ap-southeast-2:
      Standard: ami-02a3a2536c36f7f57

Resources:

  #============================================================================#
  # Control plane
  #============================================================================#

  ControlPlane:
    Type: AWS::EKS::Cluster
    Properties:
      Name: !Ref EKSClusterName
      Version: "1.26"
      RoleArn: !GetAtt ControlPlaneRole.Arn
      ResourcesVpcConfig:
        SecurityGroupIds:
        - !Ref ControlPlaneSecurityGroup
        SubnetIds:
        - !Ref VPC1PrivateSubnet1
        - !Ref VPC1PrivateSubnet2
  ControlPlaneRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          Effect: Allow
          Principal:
            Service:
            - eks.amazonaws.com
          Action: sts:AssumeRole
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/AmazonEKSClusterPolicy
      - arn:aws:iam::aws:policy/AmazonEKSServicePolicy

  #============================================================================#
  # Control plane security group
  #============================================================================#

  ControlPlaneSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for the elastic network interfaces between the control plane and the worker nodes
      VpcId: !Ref VPC1
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-ControlPlaneSecurityGroup"

  ControlPlaneIngressFromWorkerNodesHttps:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow incoming HTTPS traffic (TCP/443) from worker nodes (for API server)
      GroupId: !Ref ControlPlaneSecurityGroup
      SourceSecurityGroupId: !Ref WorkerNodesSecurityGroup
      IpProtocol: tcp
      ToPort: 443
      FromPort: 443
  ControlPlaneEgressToWorkerNodesKubelet:
    Type: AWS::EC2::SecurityGroupEgress
    Properties:
      Description: Allow outgoing kubelet traffic (TCP/10250) to worker nodes
      GroupId: !Ref ControlPlaneSecurityGroup
      DestinationSecurityGroupId: !Ref WorkerNodesSecurityGroup
      IpProtocol: tcp
      FromPort: 10250
      ToPort: 10250
  ControlPlaneEgressToWorkerNodesHttps:
    Type: AWS::EC2::SecurityGroupEgress
    Properties:
      Description: Allow outgoing HTTPS traffic (TCP/443) to worker nodes (for pods running extension API servers)
      GroupId: !Ref ControlPlaneSecurityGroup
      DestinationSecurityGroupId: !Ref WorkerNodesSecurityGroup
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443

  #============================================================================#
  # Worker nodes security group
  # Note: default egress rule (allow all traffic to all destinations) applies
  #============================================================================#

  WorkerNodesSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for all the worker nodes
      VpcId: !Ref VPC1
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-WorkerNodesSecurityGroup"
      - Key: !Sub "kubernetes.io/cluster/${ControlPlane}"
        Value: "owned"
  WorkerNodesIngressFromWorkerNodes:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow all incoming traffic from other worker nodes
      GroupId: !Ref WorkerNodesSecurityGroup
      SourceSecurityGroupId: !Ref WorkerNodesSecurityGroup
      IpProtocol: "-1"
  WorkerNodesIngressFromControlPlaneKubelet:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow incoming kubelet traffic (TCP/10250) from control plane
      GroupId: !Ref WorkerNodesSecurityGroup
      SourceSecurityGroupId: !Ref ControlPlaneSecurityGroup
      IpProtocol: tcp
      FromPort: 10250
      ToPort: 10250
  WorkerNodesIngressFromControlPlaneHttps:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow incoming HTTPS traffic (TCP/443) from control plane (for pods running extension API servers)
      GroupId: !Ref WorkerNodesSecurityGroup
      SourceSecurityGroupId: !Ref ControlPlaneSecurityGroup
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443

  #============================================================================#
  # Worker nodes (auto-scaling group)
  #============================================================================#

  WorkerNodesAutoScalingGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: 1
        MaxBatchSize: 1
    Properties:
      LaunchConfigurationName: !Ref WorkerNodesLaunchConfiguration
      MinSize: !Ref NumWorkerNodes
      MaxSize: !Ref NumWorkerNodes
      VPCZoneIdentifier:
      - !Ref VPC1PrivateSubnet1
      - !Ref VPC1PrivateSubnet1
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-WorkerNodesAutoScalingGroup"
        PropagateAtLaunch: true
      # Without this tag, worker nodes are unable to join the cluster:
      - Key: !Sub "kubernetes.io/cluster/${ControlPlane}"
        Value: "owned"
        PropagateAtLaunch: true
  WorkerNodesRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action: sts:AssumeRole
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy
      - arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy
      - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly

  # IMPORTANT NOTE: We have to define NodeGroup (type: AWS::EKS::Nodegroup), without this no woker nodes will be attach to cluster
  WorkerNodegroup:
    Type: AWS::EKS::Nodegroup
    DependsOn: ControlPlane
    Properties:
      ClusterName: !Sub "${AWS::StackName}"
      NodeRole: !GetAtt WorkerNodesRole.Arn
      ScalingConfig:
        MinSize:
          Ref: NumWorkerNodes
        DesiredSize:
          Ref: NumWorkerNodes
        MaxSize:
          Ref: NumWorkerNodes
      Subnets:
      - !Ref VPC1PrivateSubnet1
      - !Ref VPC1PrivateSubnet2
  WorkerNodesLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    # Wait until cluster is ready before launching worker nodes
    DependsOn: ControlPlane
    Properties:
      AssociatePublicIpAddress: false
      IamInstanceProfile: !Ref WorkerNodesInstanceProfile
      ImageId: !FindInMap
      - EksAmiIds
      - !Ref AWS::Region
      - Standard
      InstanceType: !Ref WorkerNodesInstanceType
      SecurityGroups:
      - !Ref WorkerNodesSecurityGroup
  WorkerNodesInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
      - !Ref WorkerNodesRole
